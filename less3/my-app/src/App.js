import React, { Component } from 'react';
import './App.css';
import {Toggler, TogglerItem} from './Toggler';
import CustomInput from './CustomInput';
import File from './File';

class App extends Component {
  constructor(props){
    super(props);
    
    this.state = {
      gender : "",
      layout : "",
      name : "",
      password : "",
      age : "",
      language : "",
      image : "https://bipbap.ru/wp-content/uploads/2017/10/0_8eb56_842bba74_XL-640x400.jpg",
    }
  }
  
  inputRef = React.createRef();
  
  changeStatus = (event) => {
     let newValue = event.target.dataset.value;
     let typeToggle = event.target.dataset.name;

     this.setState({
      [typeToggle]:  newValue
     })

  }

  handlerForm = (e) => {
    let value = e.target.value;
    let name_input = e.target.name;
    this.setState( {[name_input] : value} )
  }
  
  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state);
  }
  
  clearHandler = () => {
    for(let key in this.state){
      this.setState( {[key]:""} )
    }
  }

  fileHandler = (e) => {
    let file = e.target.files[0];
      var reader = new FileReader();
      
      reader.onload = (e) => {
      this.setState({image: reader.result})
  };

  reader.readAsDataURL(file);
  }

  StartAnim = (e) => {
    this.inputRef.current = e.target;
    this.inputRef.current.classList.add('pulse');
  }

  render() {
    let {gender, layout, name, password, age, language, image} = this.state;
    const {changeStatus} = this;
    const {handlerForm, handleSubmit, clearHandler, StartAnim, fileHandler} = this;
    return (
        <div>
           <h1>Моя Форма</h1>
            <form onSubmit={handleSubmit}>
              <CustomInput
                   name_label="Имя"
                   name_input = "name"
                   placeholder="Имя" 
                   type="text"
                   value={name}
                   typeEvent="onChange"
                   handler={handlerForm}
                />
                <CustomInput
                   name_label="Пароль"
                   name_input = "password"
                   placeholder="Пароль" 
                   type="password"
                    value={password}
                    typeEvent="onChange"
                    handler={handlerForm}
                />
              <Toggler
                  label_name="Gender"
                  name="gender"
                  changeStatus={changeStatus}
                  activeToggler={gender}

              >
                  <TogglerItem  name="male"/>
                  <TogglerItem  name="female"/>
              </Toggler>

                <CustomInput
                   name_label="Возраст"
                   name_input = "age"
                   placeholder="Возраст" 
                   type="number"
                   value={age}
                   typeEvent="onChange"
                   handler={handlerForm}
                />

                <Toggler
                  label_name="Layout"
                  name="layout"
                  changeStatus={changeStatus}
                  activeToggler={layout}
              >
                  <TogglerItem  name="left"/>
                  <TogglerItem  name="center"/>
                  <TogglerItem  name="right"/>
                  <TogglerItem  name="baseline"/>
              </Toggler>

                <CustomInput
                   name_label="Любимый язык"
                   name_input = "language"
                   placeholder="Любимый язык" 
                   type="text"
                   value={language}
                   typeEvent="onChange"
                   handler={handlerForm}
                />
                
                <button type="submit">Отправить</button>

                <CustomInput
                   type="button"
                   value="Сбросить"
                   typeEvent="onClick"
                   handler={clearHandler}
                />

                <File 
                  handler={fileHandler}
                  image={image}
                />

              </form>

              <div className="Homework1">
                  <p>Homework1</p>
                  <button 
                    ref={this.inputRef}
                    className="buttonStyle"
                    onClick={StartAnim}
                  >My Button</button> 
                  
                  <button 
                    ref={this.inputRef}
                    className="buttonStyle"
                    onClick={StartAnim}
                  >My Button2</button>
                  
                  <button 
                    ref={this.inputRef}
                    className="buttonStyle"
                    onClick={StartAnim}
                  >My Button3</button>                
            </div>
        </div>
      
    );
  }
}

export default App;
