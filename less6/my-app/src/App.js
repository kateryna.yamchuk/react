import React, { Component } from 'react';
import './App.css';
import {Provider} from 'react-redux';
import store from './redux/store';
import  {Switch, Route, Link, BrowserRouter} from 'react-router-dom';
import CharactersList from './components/CharactersList';
import Character from './components/Character';
import LocationItem from './components/LocationItem';
import EpisodeItem from './components/EpisodeItem';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div className="App">
          <Switch>
            <Route exact path="/" render= { () => (<h1><Link to="/characters">ALL Characters</Link></h1>)}/>
            <Route exact path="/characters" component={CharactersList}/>
            <Route path="/characters/:itemid" component={Character}/>
            <Route path="/location/:itemid" component={LocationItem}/>
            <Route path="/episode/:itemid" component={EpisodeItem}/>
           </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
/*
  Задание:
  1. Создать редьюсер для ваших данных (сообщение) с состояниями:
    loading
    loaded
    data
    error

  2. Создть компонент в который передавать эти данные через connect.
  3. Создать ActionCreator в котором будет находится fetchRequest на получение данных с API
  4. Вызывать этот экшн в componentDidMount вашегосозданого компонента.
  5. Рендерить список полученных данных



  https://rickandmortyapi.com/api/location/


  Rick And Morty Handbook

  ReducersMap -> 

    characters
    | lists
    | single
    episodes
    | list
    | single


  AppMap ->
    / -> welcome screen
    /characters -> Список из лоакций достпных в сериале. Выводим информацию: имя, тип, и количество резидентов
    https://rickandmortyapi.com/api/character/
    
    Используем те роуты, которые апи использует для пагинации, для реализации одного из вариантов:
      а) Постраничная пагинация 1,2,3,4
      б) Кнопка "показать еще" которая делает запрос на следующую страницу и подгружает новые данные внизу вашего компонента
    

    /characters/:characterid -> выводим информацию про персонажа, выводим детальную инфу из запроса:
    https://rickandmortyapi.com/api/character/2

    name, 
    gender, 
    origin, 
    avatar,
    episodes -> link to /episode/12
    location -> link to /locations/id

    /episode/:episodeid -> выводим информацию про эпизод:
    https://rickandmortyapi.com/api/episode/28
    Название, 
    дата выхода -> Подключите библиотеку momentjs или ее аналог
    characters -> link to : /characters/1

    /location/:locationid -> выводим информацию про локацию
    https://rickandmortyapi.com/api/location/3

  
    name
    type
    dimension
    residents -> link to: /characters/1

    В каждом из случаев:
    character
    episode
    location

    предусмотреть ответ, когда результат такого поиска не найден:
    https://rickandmortyapi.com/api/character/19239
    https://rickandmortyapi.com/api/location/31412
    https://rickandmortyapi.com/api/character/19239


    * задание со звездочкой:
    - Написать миддилвер на получение локации, в которых список residents будет заменен с массива из ссылок, на массив с обьектами characters
      Тут помогут регулярки и запрос на получение нескольких персонажей сразу:  
    https://rickandmortyapi.com/documentation/#get-multiple-characters

    Вывести их в компонент с аватарками.*/
