import React from 'react';
import { connect } from 'react-redux';
import {GetCharacters} from '../actions';

const Paginator = ( {active, number, clickHandler} ) => {
	return(
		<div className={active ? "paginationDiv active" : "paginationDiv" } onClick={clickHandler}>
			{number}
		</div>
	)
}


const mapStateToProps = (state, ownProps) => ({
	active : state.dataReducer.active === ownProps.number ? true : false
})

const mapDispatchToProps = (dispatch, ownProps) => ({
	
	clickHandler : () => {
		dispatch( GetCharacters(ownProps.number) );
	}
});


export default connect(mapStateToProps, mapDispatchToProps)(Paginator);