import React, { Component } from 'react';
import { connect } from 'react-redux';
import {getLocationItem} from '../actions'
import  {Link} from 'react-router-dom';

class LocationItem extends Component{
	
	componentDidMount = () => {
		this.props.fetchData();
	}

	render = () => {

		const {location, loaded, list_characters, loaded_residents} = this.props;
		return(
			<>
				{
					loaded === false ? (<div>Loading...</div>) :
					(
						<>
							<h2>{location.name}</h2>
							<p> <b>Type:</b> {location.type}</p>
							<p> <b>Dimension:</b> {location.dimension}</p>
							<p> <b>Created:</b> {location.created}</p>
							<h3>List of residents: </h3>
							<ul>
								{
									loaded_residents === true ? list_characters.map ( item => (<li key={item.id}><Link to={`/characters/${item.id}`}>{item.name}</Link></li>))
									: (<div>Loading...</div>)
								}
							</ul>
						</>
					)
				}
			</>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	location : state.characterReducer.location,
	list_characters : state.characterReducer.list,
	loaded_residents : state.characterReducer.loaded,
})
	
const mapDispatchToProps = (dispatch, ownProps) => ({

	fetchData : () => {
		dispatch ( getLocationItem (ownProps.match.params.itemid))
	}

})


export default connect(mapStateToProps, mapDispatchToProps)(LocationItem);