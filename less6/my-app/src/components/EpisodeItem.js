import React, { Component } from 'react';
import { connect } from 'react-redux';
import {getEpisode} from '../actions'
import  {Link} from 'react-router-dom';

class Character extends Component{
	
	componentDidMount = () => {
		this.props.fetchData();
	}

	render = () => {

		const {data,loaded,list_characters,loaded_characters} = this.props;
		return(
			<>
				{
					loaded === false ? (<div>Loading...</div>) :
					(
						<>
							<h2>{data.name}</h2>
							<p> <b>Date:</b> {data.air_date}</p>
							<p> <b>Gender:</b> {data.air_date}</p>
							<p> <b>Episode:</b> {data.episode}</p>
							<p> <b>Created:</b> {data.created}</p>
							<h3>List of Characters: </h3>
							<ul>
								{
									loaded_characters === true ? list_characters.map ( item => (<li key={item.id}><Link to={`/characters/${item.id}`}>{item.name}</Link></li>))
									: (<div>Loading...</div>)
								}
							</ul>
						</>
					)
				}
			</>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	data : state.episodeReducer.single,
	loaded : state.episodeReducer.loaded,
	list_characters : state.characterReducer.list,
	loaded_characters : state.characterReducer.loaded,
})
	
const mapDispatchToProps = (dispatch, ownProps) => ({

	fetchData : () => {
		dispatch ( getEpisode(ownProps.match.params.itemid))
	}

})


export default connect(mapStateToProps, mapDispatchToProps)(Character);