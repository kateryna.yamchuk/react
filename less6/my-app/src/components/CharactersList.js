import React, { Component } from 'react';
import { connect } from 'react-redux';
import {GetCharacters} from '../actions';
import PaginatorDiv from './PaginatorDiv';
import  {Link} from 'react-router-dom';

class CharactersList extends Component{

    componentDidMount(){
        this.props.fetchData();
    }

    render(){
        const { loaded, data} = this.props;
        return(
            <div>
                <h2>Characters</h2>
                {
                    loaded === false ?
                        (
                            <div> Loading... </div>
                        ) :
                        (
                            <>
                            	
	                            	<>
		                                {
		                                    data.map( item => {
		                                        return ( 
		                                            <div key={item.id}>
		                                                <h4><Link to={{pathname:`/characters/${item.id}`,}}>{item.name}</Link></h4>
		                                                <p>{item.species}<br/>
		                                                   {item.location.name}<br/>
		                                                Number of episodes: {item.episode.length}</p>
		                                            </div>);
		                                    })
		                                }
		                                
		                            </>
		                        
	                            <div className="paginationContainer">
	                            	<PaginatorDiv number={1}/>
	                            	<PaginatorDiv number={2}/>
	                            	<PaginatorDiv number={3}/>
	                            	<PaginatorDiv number={4}/>
	                            </div>
                            </>
                        )
                }
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
	data: state.dataReducer.data,
    loaded: state.dataReducer.loaded,
})
	
const mapDispatchToProps = (dispatch, ownProps) => ({


    fetchData: () => {
    	dispatch( GetCharacters(1) );
    }
});


export default connect(mapStateToProps, mapDispatchToProps)(CharactersList);
