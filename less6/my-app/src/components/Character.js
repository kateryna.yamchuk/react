import React, { Component } from 'react';
import { connect } from 'react-redux';
import {getItemCharacter} from '../actions'
import  {Link} from 'react-router-dom';

class Character extends Component{
	
	componentDidMount = () => {
		this.props.fetchData();
	}

	render = () => {

		const {data,loaded, episode_list, location_id, loaded_episode_list} = this.props;
		return(
			<>
				{
					loaded === false ? (<div>Loading...</div>) :
					(
						<>
							<h2>{data.name}</h2>
							<p> <b>Species:</b> {data.species}</p>
							<p> <b>Gender:</b> {data.gender}</p>
							<p> <b>Origin:</b> {data.origin.name}</p>
							<p> <b>Location:</b> <Link to={`/location/${location_id}`}>{data.location.name}</Link></p>
							<img src={data.image} alt="some image"/>
							<h3>List of episodes: </h3>
							<ul>
								{
									loaded_episode_list === true ? episode_list.map( item => (<li key={item.id}><Link to={`/episode/${item.id}`}>{item.name}</Link></li>))
									: (<div>Loading...</div>)
								}
							</ul>
						</>
					)
				}
			</>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	data : state.characterReducer.data,
	loaded : state.characterReducer.loaded,
	episode_list : state.episodeReducer.list,
	loaded_episode_list : state.episodeReducer.loaded,
	location_id : state.characterReducer.location_id,
})
	
const mapDispatchToProps = (dispatch, ownProps) => ({

	fetchData : () => {
		dispatch ( getItemCharacter (ownProps.match.params.itemid))
	}

})


export default connect(mapStateToProps, mapDispatchToProps)(Character);