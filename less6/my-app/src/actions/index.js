 export const DATA_REQ = 'DATA_REQ';
 export const DATA_RES = 'DATA_RES';
 export const DATA_REQ_EPISODE = 'DATA_REQ_EPISODE';
 export const EPISODE_ADD = 'EPISODE_ADD';
 export const NEW_ADDING = 'NEW_ADDING';
 export const EPISODE_RES = 'EPISODE_RES';
 export const EPISODE_RES_END = 'EPISODE_RES_END';
 export const DATA_CHARACTER_REQ = 'DATA_CHARACTER_REQ';
 export const DATA_CHARACTER_RES = 'DATA_CHARACTER_RES';
 export const LOCATION_ID = 'LOCATION_ID';
 export const GET_LOCATION = 'GET_LOCATION';
 export const CHARACTER_ADD = 'CHARACTER_ADD';
 export const NEW_CHARACTER_ADDING = 'NEW_CHARACTER_ADDING';
 export const CHARACTER_RES_END = 'CHARACTER_RES_END';

export const GetCharacters = (page) => ( dispatch, getState ) => {
    dispatch({type: DATA_REQ });
    fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
        .then( res => res.json() )
        .then( res => {
        	return dispatch({ type: DATA_RES, payload: { data : res.results, page : page} })
        })
}

export const getItemCharacter = (id) => async ( dispatch, getState ) => {
    dispatch({type: DATA_CHARACTER_REQ });
    const context = {};
    fetch(`https://rickandmortyapi.com/api/character/${id}`)
        .then( res => res.json() )
        .then( res => {	
        	context.res = res;
        	dispatch({ type: DATA_CHARACTER_RES, payload: res })
        	dispatch({type : NEW_ADDING})
        	
        	let promise = Promise.resolve();
        	
        	res.episode.forEach( item => {
    
        		promise = promise.then( async () => {
					await fetch(item)
					.then( res => res.json() )
        			.then( res => {
        				return dispatch({ type: EPISODE_ADD, payload: res })
        			})
        		})
        	})
        	return promise.then( () => null);

		})
		.then( () => {	
        	dispatch({type : EPISODE_RES_END});
        	fetch(context.res.location.url)
        		.then( res => res.json() )
        		.then( res => {
        			return dispatch ({ type: LOCATION_ID, payload: res.id })
        		})
		})
}

export const getLocationItem = (id) => ( dispatch, getState ) => {
	dispatch({type: DATA_CHARACTER_REQ });
    fetch(`https://rickandmortyapi.com/api/location/${id}`)
        .then( res => res.json() )
        .then( res => {	
        	dispatch({ type: GET_LOCATION, payload: res })
        	dispatch({type : NEW_CHARACTER_ADDING})

        	let promise = Promise.resolve();
        	res.residents.forEach( item => {

        		/*fetch(item)
        			.then( res => res.json() )
        			.then( res => {
        				return dispatch({ type: CHARACTER_ADD, payload: res })
        			})*/
        		promise = promise.then( async () => {
					await fetch(item)
					.then( res => res.json() )
        			.then( res => {
        				return dispatch({ type: CHARACTER_ADD, payload: res })
        			})
        		})
        	})
        	return promise.then( () => null);
        })
        .then( () => dispatch({type:CHARACTER_RES_END}))
}


export const getEpisode = (id) => (dispatch, getState) => {
	dispatch({type: DATA_REQ_EPISODE});
    fetch(`https://rickandmortyapi.com/api/episode/${id}`)
        .then( res => res.json() )
        .then( res => {	
        	dispatch({ type: EPISODE_RES, payload: res })
        	dispatch({type : NEW_CHARACTER_ADDING})

        	let promise = Promise.resolve();
        	res.characters.forEach( item => {
        		/*fetch(item)
        			.then( res => res.json() )
        			.then( res => {
        				return dispatch({ type: CHARACTER_ADD, payload: res })
        			})*/
        		promise = promise.then( async () => {
					await fetch(item)
					.then( res => res.json() )
        			.then( res => {
        				return dispatch({ type: CHARACTER_ADD, payload: res })
        			})
        		})
        	})
        	return promise.then( () => null);
        })
        .then( () => dispatch({type:CHARACTER_RES_END}))
}


