import { 
  DATA_REQ_EPISODE,
  EPISODE_ADD,
  NEW_ADDING,
  EPISODE_RES,
  EPISODE_RES_END
} from '../actions';

const dataInitialState = {
    list : [],
    loaded : false,
    single : {},
};


const episodeReducer = ( state = dataInitialState, action) => {
  switch( action.type ){
      case DATA_REQ_EPISODE:
        return{
          ...state,
          loaded: false
        }

      case EPISODE_ADD:
        return{
          ...state,
          loaded: false,
          list: [...state.list, action.payload],
        }
      
      case NEW_ADDING:
        return{
          ...state,
          loaded: false,
          list: []
        }

      case EPISODE_RES:
        return{
          ...state,
          loaded : true,
          single: action.payload,
        }

      case EPISODE_RES_END:
        return{
          ...state,
          loaded: true,
        }

      default:
        return state;
    }
}


export default episodeReducer;