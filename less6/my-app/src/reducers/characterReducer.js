import { 
  DATA_CHARACTER_REQ,
  DATA_CHARACTER_RES,
  LOCATION_ID,
  GET_LOCATION,
  CHARACTER_ADD,
  NEW_CHARACTER_ADDING,
  CHARACTER_RES_END,
} from '../actions';


const dataInitialState = {
    data : {},
    list : [],
    loaded : false,
    error : [],
    location_id : 0,
    location : {},
};


const characterReducer = ( state = dataInitialState, action) => {
  switch( action.type ){
      case DATA_CHARACTER_REQ:
        return{
          ...state,
          loaded: false
        }

      case DATA_CHARACTER_RES:
        return{
          ...state,
          loaded: true,
          data: action.payload
        }

      case LOCATION_ID:
        return{
          ...state,
          location_id : action.payload,
        }

      case GET_LOCATION:
        return{
          ...state,
          loaded: true,
          location: action.payload
        }

        case CHARACTER_ADD:
        return{
          ...state,
          loaded: false,
          list: [...state.list, action.payload],
        }
      
      case NEW_CHARACTER_ADDING:
        return{
          ...state,
          loaded: false,
          list: []
        }
      
      case CHARACTER_RES_END:
        return{
          ...state,
          loaded : true
        }

      default:
        return state;
    }
}


export default characterReducer;