import { 
 DATA_REQ,
  DATA_RES,
} from '../actions';



const dataInitialState = {
    loaded : false,
    data : [],
    active : 0,
};


const dataReducer = ( state = dataInitialState, action) => {
  switch( action.type ){
      case DATA_REQ:
        return{
          ...state,
          loaded: false
        }

      case DATA_RES:
        return{
          ...state,
          loaded: true,
          data: action.payload.data,
          active : action.payload.page,
        }
  
      default:
        return state;
    }
}


export default dataReducer;
