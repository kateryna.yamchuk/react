import { combineReducers } from 'redux';

import dataReducer from './dataReducer';
import characterReducer from './characterReducer';
import episodeReducer from './episodeReducer'

const reducer = combineReducers({
  dataReducer,
  characterReducer,
  episodeReducer,
});

export default reducer;

