const InitialState = {
    type : "login",
    current_user : {},
    login_user : {
      email : "",
      password : "",
    },
    list : [],
    loading_list : false,
    cur_member : {},
    loading_member : false,
  };
  
  const reducer1 = ( state = InitialState, action) => {
    switch( action.type ){
      
      case 'REGISTRATION':
        return{
          ...state,
          type : "registration"
        }

      case 'REGISTRATION_END':
        return{
          ...state,
          type : "login"
        }

      case 'FIELD_DATA' :

        return{
          ...state,
          login_user : {...state.login_user, [action.payload.name] : action.payload.value}
        }

      case 'LOGIN_USER' :
        return{
          ...state,
          current_user : action.payload,
          type : "app",
        }
      
      case 'AXIOS_DATA' :
        return{
          ...state,
          loading_list : true,
          list : action.payload,
        } 

      case 'AXIOS_DATA_MEM' :
        return{
          ...state,
          loading_member : true,
          cur_member : action.payload,
        } 

      case 'BACK_LOGIN' :
        return{
          ...state,
          type : "login",
        }  

      default:
        return state;
    }
  }
  
  
  export default reducer1;
  
