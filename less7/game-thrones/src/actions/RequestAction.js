import axios from 'axios';

export const axiosData = () => (dispatch, getState ) => {
	axios.post('http://www.json-generator.com/api/json/get/bUzeVobdfm?indent=2')
		.then(response => {
			let data = response.data.filter( item => {
				return item.house === getState().reducer1.current_user.house
			})
			dispatch({type : 'AXIOS_DATA', payload: data});
		})
}
   
export const axiosDataMem = (index) => (dispatch, getState ) => {
	axios.post('http://www.json-generator.com/api/json/get/bUzeVobdfm?indent=2')
		.then(response => {
			
			let data = response.data.find( item => {
				
				return item.index+"" === index;
			})

			dispatch({type : 'AXIOS_DATA_MEM', payload: data});
		})
}