import React from 'react';
import { Provider } from 'react-redux';
import './Root.css';
import store from './redux/store'
import MyApp from './components/MyApp'

function Root() {
  return (
    <div className="Root">
      <Provider store={store}>
        <MyApp/>
      </Provider>
    </div>
  );
}

export default Root;
