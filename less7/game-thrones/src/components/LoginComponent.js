import React from 'react';
import { connect } from 'react-redux';
import LoginUser from '../actions/AuthorizationActions';
import './Form.css';

class LoginComponent extends React.Component{

	render = () => {
		const {changeToReg, handlerChange, submitHandler} = this.props;
		return(
			<div className="loginForm">
				<form onSubmit={submitHandler}>
					<div>
						<input type="text" placeholder="email" onChange={handlerChange} name="email"/>
					</div>
					<div>
						<input type="text" placeholder="password" onChange={handlerChange} name="password"/>
					</div>
					<div>
						<input type="submit" value="Log in"/>
					</div>
					<div>
						<div><p onClick={changeToReg}>Registration</p></div>
					</div>
				</form>
			</div>
		)
	}
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
   
   changeToReg : (e) => {
   	dispatch( {type: 'REGISTRATION'} ) 
   },

   handlerChange : (e) => {
   	 let value = e.target.value;
	 let name = e.target.name;
	 dispatch( {type: 'FIELD_DATA', payload : {value : value, name : name}} )
   }, 

   submitHandler : (e) => {
   	 e.preventDefault();
   	 dispatch( LoginUser());
   }
});

export default connect(null, mapDispatchToProps)(LoginComponent);
