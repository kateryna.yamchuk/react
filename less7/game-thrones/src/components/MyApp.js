import React from 'react';
import { connect } from 'react-redux';
import LoginComponent from './LoginComponent';
import RegistrationComponent from './RegistrationComponent';
import ApplicationComponent from './ApplicationComponent';


export const ThemedContext = React.createContext();

const MyApp = ( {typeOfPage, house} ) => {
	return(
		<>
			
				{
					typeOfPage === "login" ? (<LoginComponent/>) : 
					typeOfPage === "registration" ? (<RegistrationComponent/>) : 
					(	
						<ThemedContext.Provider value={house}>
							<ApplicationComponent/>
						</ThemedContext.Provider>		          		
					)
				}
			
		</>
	)
}


const mapStateToProps = (state, ownProps) => 
    ({
        typeOfPage : state.reducer1.type,
        house : state.reducer1.current_user.house,
    });

export default connect(mapStateToProps, null)(MyApp);