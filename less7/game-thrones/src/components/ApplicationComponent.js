import React from 'react';
import './ApplicationComponent.css'
//import { connect } from 'react-redux';
import {ThemedContext} from './MyApp'
import  {Switch, Route, Link, BrowserRouter} from 'react-router-dom';
import MembList from './MembList';
import MemberComp from './MemberComp'; 
import Helmet from 'react-helmet';
import favicon1 from '../images/favicon-16x16.ico';

const ApplicationComponent = () => {
	return(	
	  <BrowserRouter>
		<ThemedContext.Consumer>
            {
                house => (
                    <div className={`${house}_style_body`}>
                    	
                    	<Helmet>
                    		<meta charset="utf-8" />
        					<title>{house}</title>
        					<link rel="shortcut icon" href={favicon1}/>
    					</Helmet>

                        <div className={`style_logo ${house}`}></div>
                        <div className="content_style">
                        	<div>
                        		<h1>{house+' House'}</h1>
                        	</div>
                        	<Route exact path="/" render= { () => (
                     
                        	<div>
                        		{
								   house === "Stark" ? 
									   (<p>House Stark of Winterfell is a Great House of Westeros, 
									   	ruling over the vast region known as the North from their seat in Winterfell. 
									   	It is one of the oldest lines of Westerosi nobility by far, 
									   	claiming a line of descent stretching back over eight thousand years. 
									   	Before the Targaryen conquest, 
									   	as well as during the War of the Five Kings and Daenerys Targaryen's invasion of Westeros, 
									   	the leaders of House Stark ruled over the region as the Kings in the North.</p>) :
								    house === "Lannister" ?
									    (<p>House Lannister of Casterly Rock is one of the Great Houses of Westeros, 
									    one of its richest and most powerful families and oldest dynasties. 
									    It is also the current royal house of the Seven Kingdoms following the extinction of House Baratheon of King's Landing, 
									    which had been their puppet house during the War of the Five Kings.</p>) :
									house === "Arryn" ? 
										(<p>House Arryn of the Eyrie is one of the Great Houses of Westeros. 
										It has ruled over the Vale of Arryn for millennia, 
										originally as the Kings of Mountain and Vale and more recently as Defenders of the Vale and Wardens of the East under the Targaryen, 
										Baratheon, and Lannister dynasties. The nominal head of House Arryn is Robin Arryn, 
										the Lord of the Eyrie, with Yohn Royce holding actual power over the house.</p>) :
									house === "Greyjoy" ? 
										(<p>House Greyjoy of Pyke is one of the Great Houses of Westeros. 
										It rules over the Iron Islands, a harsh and bleak collection of islands off the west coast of Westeros, 
										from the castle at Pyke. The head of the house is the Lord Reaper of Pyke.
										House Greyjoy's sigil is traditionally a golden kraken on a black field. 
										Their house words are "We Do Not Sow," 
										although the phrase "What Is Dead May Never Die" is also closely associated with House Greyjoy and their bannermen, 
										as they are associated with the faith of the Drowned God.</p>)
									:
										(<p>House Targaryen of Dragonstone is a Great House of Westeros and was the ruling royal House of the Seven Kingdoms for three centuries 
											since it conquered and unified the realm before it was deposed during Robert's Rebellion and House Baratheon replaced it as the new royal House. 
											The two surviving Targaryens fled into exile to the Free Cities of Essos across the Narrow Sea. 
											Currently based on Dragonstone off of the eastern coast of Westeros, 
											House Targaryen seeks to retake the Seven Kingdoms from House Lannister, 
											who formally replaced House Baratheon as the royal House following the destruction of the Great Sept of Baelor.</p>)
								}
								<div>
									<Link to="/users"><p>{"Members of the House "+house}</p></Link>
								</div>
							</div>
							)}/>
							<Route exact path="/users" component={MembList}/>
							<Route path="/users/:index" component={MemberComp}/>
                        </div>
                    </div>
                )
            }
        </ThemedContext.Consumer>
       </BrowserRouter>
	)
}

export default ApplicationComponent;
