import React from 'react';
import { connect } from 'react-redux';

class RegistrationComponent extends React.Component{

	state = {
		data : {
			name : "",
			email : "",
			house : "",
			password : "",
		}
	}
		
	handleSubmit = (event) => {
		event.preventDefault();
		//localStorage.removeItem('users');
		let users = localStorage.getItem('users');
		if (users === null) {
			users = [this.state.data];
		} else {
			users = JSON.parse(users);
			users.push(this.state.data);
		}
		localStorage.setItem('users',JSON.stringify(users));
		alert('Registration Successful')
		this.props.backToLog();
	}

	handleOnChange = (event) => {
		let value = event.target.value;
    	let name = event.target.name;

    	this.setState({
	      data:{
	        ...this.state.data,
	        [name]: value,
	      }});
	}

	render = () => {
		const {handleBack} = this.props;
		return(
			<div className="registrationForm">
				<form onSubmit={this.handleSubmit}>
					<div>
						<input type="text" onChange={this.handleOnChange} name="name" placeholder="name"/>
					</div>
					<div>
						<input type="email" onChange={this.handleOnChange} name="email" placeholder="email"/>
					</div>
					<div>
						<p><select size="3" onChange={this.handleOnChange} multiple name="house">
						    <option disabled>Selection of House</option>
						    <option value="Stark">Stark</option>
						    <option value="Targaryen">Targaryen</option>
						    <option value="Lannister">Lannister</option>
						    <option value="Greyjoy">Greyjoy</option>
						    <option value="Arryn">Arryn</option>
			   			</select></p>
			   		</div>
			   		<div>
						<input type="password" onChange={this.handleOnChange} name="password" placeholder="password"/>
					</div>
					<div>
						<button type="submit">Register</button>
					</div>
					<div>
						<div onClick={handleBack}>Back</div>
					</div>
				</form>
			</div>	
		)
	}
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
   backToLog : () => {
   	 dispatch( {type: 'REGISTRATION_END'});
   },

   handleBack : () => {
   	 dispatch( {type: 'BACK_LOGIN'});
   }
});

export default connect(null, mapDispatchToProps)(RegistrationComponent);
