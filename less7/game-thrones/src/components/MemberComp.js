import React from 'react';
import { connect } from 'react-redux';

import {axiosDataMem} from '../actions/RequestAction'

class MemberComp extends React.Component{
	
	componentDidMount(){
		this.props.getUser();
	}


	render() {
		const {cur_member, loading_member} = this.props;
		return (
			<>
				{
					loading_member === false ? (<div>Loading...</div>) :
					 (
						<div>
						   <p>Hello, {cur_member.name}, son of {cur_member.father}!
           					You welcome here!
           				   </p>
						</div>
					 )
				}
			</>
		)
	}
}

const mapStateToProps = (state, ownProps) => 
    ({
    	cur_member : state.reducer1.cur_member,
    	loading_member : state.reducer1.loading_member,
    });

const mapDispatchToProps = ( dispatch, ownProps ) => ({
   	
   	getUser : () => {
   		dispatch( axiosDataMem(ownProps.match.params.index))
   	}
});

export default connect(mapStateToProps, mapDispatchToProps)(MemberComp);