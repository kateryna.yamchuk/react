import React from 'react';
import { connect } from 'react-redux';
import  {Link} from 'react-router-dom';
import {axiosData} from '../actions/RequestAction'

class MembList extends React.Component{
	
	componentDidMount(){
		this.props.getUsers();
	}


	render() {
		const {loading_list, list} = this.props;
		return (
			<>
				{
					loading_list === false ? (<div>Loading...</div>) :
					list.map( item => (<li key={item.id}><Link to={`/users/${item.index}`}>{item.name+" "+item.house}</Link></li>))
				}
			</>
		)
	}
}

const mapStateToProps = (state, ownProps) => 
    ({
    	list : state.reducer1.list,
    	loading_list : state.reducer1.loading_list
    });

const mapDispatchToProps = ( dispatch, ownProps ) => ({
   	
   	getUsers : () => {
   		dispatch( axiosData())
   	}
});

export default connect(mapStateToProps, mapDispatchToProps)(MembList);