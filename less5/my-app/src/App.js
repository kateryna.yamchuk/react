import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './redux/store';
import ToDoList from './components/ToDoList';
import  {Switch, Route, Link, BrowserRouter} from 'react-router-dom';
import DoneList from './components/DoneList';
import NotDoneList from './components/NotDoneList';

class App extends Component {
  render() {
    return (
     <Provider store={store}>
        <div className="App">
          <h1>To Do List</h1>
          <BrowserRouter>
            <div className="menu">
              <div><h3><Link to="/">Все</Link></h3></div>
              <div><h3><Link to="/false">Не выполненные</Link></h3></div>
              <div><h3><Link to="/true">Выполненные</Link></h3></div>
            </div>
            <>
              <Switch>
                <Route exact path="/" component={ToDoList}/>
                <Route exact path="/true" component={DoneList}/>
                <Route exact path="/false" component={NotDoneList}/>
              </Switch>
            </>
          </BrowserRouter>
        </div>
     </Provider>
    );
  }
}

export default App;


/*

    Задание:
    Написать простой ToDo list, у которого есть возможность:
    - Добавить запись
    - Удалить запись
    - Отметить запись выполненной

    + бонус.
    Должно быть 3 роута которые показывают:
      1. Все записи
      2. Выполненные записи
      3. Не выполненные записи

    Все это с использованием Redux.

*/
