const initialState = {
    list : [{ id:1, title:'qweqweqe',done:true}, {id:2, title:'lalalalalala',done:false}],
	newRecValue : "",
	id : 3,
	filter_list : [],
  };


function reducer(state = initialState, action){
    switch(action.type){
    	case 'ADD_RECORD':
    		state.list.push({id:state.id, title:state.newRecValue, done:false})
    		return{
    			...state,
    			list : state.list = [...state.list],
    			newRecValue : "",
    			id : state.id + 1,
    		}

    	case 'TEXT_VALUE':
    		return{
    			...state,
    			newRecValue : state.newRecValue = action.payload.value,
    		}

    	case 'CHECK':
    		state.list.forEach ( item => {
    			if (item.id === action.payload) item.done = !item.done;
    		})
    		
    		return{
    			...state,
    			list : [...state.list],
    		}

    	case 'DELETE':
    		state.list.splice(state.list.indexOf(state.list.find( item => item.id === action.payload)),1);
    		return{
    			...state,
    			list : [...state.list],
    		}

    	case 'FILTER_LIST':
    		return{
    			...state,
    			filter_list: state.list.filter( item => item.done === action.payload )
    		}

    	default:
    		return state;
    }
  };


export default reducer;
