import React, {Component} from 'react';
import { connect } from 'react-redux';
import ListItem from './ListItem';

 class NotDoneList extends Component{
	
	componentDidMount = () =>{
		this.props.filterData();
	}
	render = () => { 
		const { filter_list} = this.props;
		return(
			<div className="ToDoList">
				<div className="List">
					<ol>
						{
							filter_list.length !== 0 ? filter_list.map( (item) => 
								(<ListItem key={item.id} title={item.title} done={item.done} id={item.id}/>)
						) : 
								(<p>There are no records</p>)
						}							
					</ol>		
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	    filter_list: state.filter_list,
	});

const mapDispatchToProps = (dispatch, ownProps) => ({
  
   	filterData: () => {   	
   		dispatch({ type: 'FILTER_LIST', payload: false})
    },
});


export default connect(mapStateToProps, mapDispatchToProps)(NotDoneList);
