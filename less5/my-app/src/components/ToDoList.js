import React, {Component} from 'react';
import { connect } from 'react-redux';
import ListItem from './ListItem';

 class ToDoList extends Component{

	render = () => {
		const { list, add, changeHandler, newRecValue } = this.props;
		return(
			<div className="ToDoList">
				<div className="List">
					<ol>
						{
							list.length !== 0 ? list.map( (item) => 
								(<ListItem key={item.id} title={item.title} done={item.done} id={item.id}/>)
						) : 
								(<p>There are no records</p>)
						}							
					</ol>		
				</div>
				<div className="ControlInputs">
					<input type="text" onChange={changeHandler} placeholder="add new record" value={newRecValue}/>
					<button onClick={add}>Add</button>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	    list: state.list,
	    newRecValue: state.newRecValue,
	    done_list: state.done_list,
	});

const mapDispatchToProps = (dispatch, ownProps) => ({
   	
   	add : (e) => {
   		dispatch( {type:'ADD_RECORD'} )
   	},
	
	changeHandler : (e) => {
   		dispatch( {type:'TEXT_VALUE', payload: {value:e.target.value} } )
   	},
});


export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
