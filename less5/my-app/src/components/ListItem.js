import React from 'react';
import { connect } from 'react-redux';

const ListItem = ({title, done, id, changeHandlerCheck, clickHandler}) => {
	return(
		<li className={done ? "done" : ""}>
			{title}
			<input type="checkbox" onChange={changeHandlerCheck} checked={done ? true : false}/>
			<button onClick={clickHandler}>Удалить</button>
		</li>
	)
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return({
   	
	   	changeHandlerCheck : (e) => {
	   		dispatch( {type:'CHECK',payload:ownProps.id} )
	   	},

	   	clickHandler : (e) => {
	   		dispatch( {type:'DELETE',payload:ownProps.id} )
	   	}
	});
}
export default connect(null, mapDispatchToProps)(ListItem);