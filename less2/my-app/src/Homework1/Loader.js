import React, { Component } from 'react';


class Loader extends Component{
	constructor(props){
		super(props);
		this.state = {
			loaded: false
		}
	}

	componentDidMount(){

		let loadedImg = new Image();

			loadedImg.onload = () => this.setState({ loaded: true });
			loadedImg.src = this.props.src;
	}

	render(){
		const { loaded } = this.state;
		if( loaded ){
			const {src} = this.props;
			return(
				<div>
					<img src={src}></img>
				</div>
			)
		} else {
			return (<div> Image is loading </div>);
		}

		
		
	}
}

export default Loader;