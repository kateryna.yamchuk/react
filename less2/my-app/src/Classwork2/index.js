import React, { Component } from 'react';
import UserComponents from './UserComponents'
import StateLessBtn from '../Classwork1/StateLessBtn'

class Classwork2 extends Component{
	constructor(props){
		super(props);
		this.state = {
			componentsArray : []
		}
	this.clickHandler = this.clickHandler.bind(this);
	}
	
	clickHandler(){
		const {componentsArray} = this.state;
		let newArray = componentsArray.map( item => {
			return ( {interviewed: !item.interviewed, user: item.user} )
		})

		this.setState( {
			componentsArray : newArray
		})
	}

	componentDidMount(){
		const url = "http://www.json-generator.com/api/json/get/ckPDzNHDAO?indent=2";
		fetch(url).then( response => 
			response.json()).then(
				data => {
					let componentsArray = data.map( user => {
						return( {interviewed: false, user} )
					})

					this.setState( {
						componentsArray : componentsArray
					})
				})
		}

	render(){
		const {componentsArray} = this.state;
		const {clickHandler} = this;
		return(
			<div>
				{componentsArray.map( (item) => (
					<UserComponents
						user = {item.user}
						interviewed = {item.interviewed}
						key = {item.user.index}
					/> 
					) 
				 )}
				<StateLessBtn 
					clickHandler = {clickHandler}
				/>
			</div>
		)
	}
}

export default Classwork2;