import React from 'react';

const StateLessBtn = ({style, clickHandler, textValue}) => {
	return(
		<button 
			style={style}  
			onClick={clickHandler}>
			{textValue}
		</button>
	)
}

StateLessBtn.defaultProps = {
  style: {
    backgroundColor: '#C0C0C0'
  },
  clickHandler: () => {
    console.log('Default action!!');
  },
  textValue: "My Button"
}

export default StateLessBtn;