import React, { Component } from 'react';

class PostItem extends Component{
	state = {
		post : null,
		comments : null,
		startLoad : false,
	}
	
	componentDidMount = () => {
		const postid = this.props.match.params.itemid;
		
		fetch(`https://jsonplaceholder.typicode.com/posts/${postid}`)
			.then( res => res.json() )
			.then( res => {

				this.setState({post:res})
		})
	}
	
	showComments = () => {
		const postid = this.props.match.params.itemid;
		fetch(`https://jsonplaceholder.typicode.com/posts/1/comments`)
			.then( res => res.json() )
			.then( res => {
				
				let commentsArr = res.filter( (item) => {
					if (item.postId == postid){
						return item;
					}
				})

                this.setState({
                	startLoad : true,
                    comments: commentsArr,
                })
		})
	}

	render = () => {
		const {post, comments, startLoad} = this.state;
		const {showComments} = this;
		return(
			<div>
				<h1>Пост</h1>
					<div>
						{
							(post === null) ? 
							  (<div>Page in loading...</div>) : 
							  (<div>
							  	 <p>{post.title}</p>
							  	 <p>{post.body}</p>
							  	 <div>
							  	 	{ !startLoad && comments === null ? 
							  	 	  (<button onClick={showComments}>Показать комментарии</button>) :
							  	 	  startLoad && comments === null ?
							  	 	  <div>Comments in loading...</div> :
							  	 	  <div>{comments.map( (item) => 
							  	 			(<li key={item.id}>{item.name}</li>))}
							  	 	  </div>
							  	 	}
							  	 </div>
							   </div>)
						}
					</div>
			</div>
		)
	}
}

export default PostItem;