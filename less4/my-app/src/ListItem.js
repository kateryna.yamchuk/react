import React, { Component } from 'react';
import  {Link} from 'react-router-dom'

class ListItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			albums: null
		}
	}

	componentDidMount = () => {
		const artistid = this.props.match.params.itemid;
		fetch(`https://jsonplaceholder.typicode.com/albums`)
            .then( res => res.json() )
            .then( res => {
				
				let curArtAlb = res.filter( (item) => {
					if (item.userId == artistid){
						return item;
					}
				})

                this.setState({
                    albums: curArtAlb
                })

            });

	}

	render = () =>{
		const {albums} = this.state;
		return(
			<div>
				<h1>List Item</h1>
					<div>
						{
						  (albums === null) ? 
						  (<div>Page in loading...</div>) : 
						  (<div> 
						  	{albums.map( item => (<li>{item.title}</li>))}
						   </div>)
						}
					</div>
			</div>
		)
	}
}


export default ListItem;