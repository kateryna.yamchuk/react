import React, { Component } from 'react';
import './App.css';
import  {BrowserRouter, Switch, Route, Link} from 'react-router-dom'

import About from './About'
import HomePage from './HomePage'
import List from './List'
import Contacts from './Contacts'
import NotFound from './NotFound'

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <header>
            <Link to="/">HomePage</Link>
            <Link to="/list">List</Link>
            <Link to="/about">About</Link>
            <Link to="/contacts">Contacts</Link>
          </header>
        <Switch>
          <Route exact path="/" component={HomePage}/>
          <Route path="/list" component={List}/>
          <Route exact path="/about" component={About}/>
          <Route exact path="/contacts" component={Contacts}/>
          <Route component={NotFound}/>
        </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;






/*

    Задание 1.

      Написать компоненты, HomePage, About, Contacts, List, Item
      По роутам:
      /             -> HomePage
      /list         -> List
      /list/:itemid -> Item
      /about        -> About
      /contacts     -> Contacts

      Настроить их коректную работу.
      Сделать общий компонент меню в котором сделать навигацию через Link
      Компоненты просто пустые и выводят просто заголовок компонента
      

Обернуть роуты с предыдущего задания в компонент Switch и добавить компонент 404.

    http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2
    http://www.json-generator.com/api/json/get/bGyYXYiCOG?indent=2
    http://www.json-generator.com/api/json/get/bTUcoMILQO?indent=2


    https://jsonplaceholder.typicode.com/users
    https://jsonplaceholder.typicode.com/albums
    https://jsonplaceholder.typicode.com/photos

    Сгенерировать список артистов исходя из любого их JSON выше.
    Дальше после перехода на артиста показывать список его альбомов и количество композиций в нем.
      + бонус, показывать общее время всех треков

    После этого возможность зайти в альбом и посмотреть все треки.
      + бонус, возможность выставить рейтинг от 1 до 5 звезд каждому треку
      + бонус, сохранять звезды в localStorage

*/