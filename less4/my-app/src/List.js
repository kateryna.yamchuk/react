import React, { Component } from 'react';
import  {Switch, Route, Link} from 'react-router-dom';
import ListItem from './ListItem'

class List extends Component {
	state = {
		list: null
	}
	
	componentDidMount = () =>{
		fetch("https://jsonplaceholder.typicode.com/users")
			.then( res => res.json() )
			.then( res => {
				this.setState({list:res})
			})
	}

	render = () => {
		const { list } = this.state;

		return(
			<div>
				<h1>List</h1>
					<div>
						{
						  (list === null) ? 
						  (<div>Page in loading...</div>) : 
						  (<div> 
						  	<Switch>
								<Route exact path="/list/" render={ () => list.map( (item) => (<li><Link to={{pathname:`/list/${item.id}`,}}>{item.name}</Link></li>))  }/>
								<Route exact path="/list/:itemid" component={ListItem}/>
							</Switch>
						   </div>)
						}
					</div>
			</div>
		)	
	}
}

export default List;